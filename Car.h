#include <iostream>
class Car
{
    int cost;
    std::string name;
    
public:
    Car();
    ~Car();
    Car(int c, std::string n);
    int getCost();
    void show();
};