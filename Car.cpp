#include "Car.h"

Car::Car() = default;
Car::~Car() = default;

Car::Car(int c, std::string n): cost(c), name(n) 
{
    std::cout << "Constructor with parameters\n";
}

void Car::show()
{
    std::cout << "Car " << name << ": " << cost << '\n';
}

int Car::getCost()
{
    return cost;
}